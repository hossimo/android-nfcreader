package com.downrighttech.nfcreader;

import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        Log.i("onCreate", intent.toString());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        performTagOperations(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("onPause","started");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("onResume","started");
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0 ,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
                0);

        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null)
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
    }
    private void performTagOperations(Intent intent) {
        if (intent.getAction() != null)
            Log.i("tagOps", intent.getAction());

        // NDEF
        Parcelable[] ndefMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

        if (ndefMessages != null){
            int count = ndefMessages.length;
        } else {
            Log.i("tagOps", "No NDEF data");
        }


        // TAG
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        String techList[] = tag.getTechList();

        LinearLayout techListLayout = findViewById(R.id.uiLayoutTechList);
        techListLayout.removeAllViewsInLayout();

        for (String t: techList) {
            TextView value = new TextView(this);
            value.setText(t);
            techListLayout.addView(value);
        }


        byte[] tagID = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
        String tagIDString = bytesToHex(tagID);

        TextView uiTagID = findViewById(R.id.uiCurrentID);
        uiTagID.setText(tagIDString);

        try {
            addUser(tagIDString, "ADNROID");
        } catch (IOException e) {
            e.printStackTrace();
        }


        Log.i("tagOps", "done");

    }

    private void addUser (String playerID, String playerName) throws IOException {
        playerName ="AdroidTest";
        URL url = new URL("http://192.168.31.119:5000/?" + playerID +"/"+ playerName);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try{
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        } finally {
            urlConnection.disconnect();
        }
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
